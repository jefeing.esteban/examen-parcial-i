const fetch = require('node-fetch');

async function obtainPendings() {
    const answer = await fetch('http://jsonplaceholder.typicode.com/todos');
    return await answer.json();
}

async function showMenu() {
    console.log('Choose an option:');
    console.log('1. List of all pendings (IDs only: )');
    console.log('2. List of all pendings (IDs and Titles):');
    console.log('3. List of pendings without resolution (ID and Title):');
    console.log('4. List of all pendings solved (ID and Titles):');
    console.log('5. List of all pendings (IDs and UserID):');
    console.log('6. List of all pendings resolved (ID and UserID):');
    console.log('7. List of all pendings without solution (ID and UserID):');
    console.log('0. Exit');

    const opc = await optionsUser();

    if (opc !== 0) {
        switch (opc) {
            case 1:
                await listIDs();
                break;
            case 2:
                await listIDsAndTitle();
                break;
            case 3:
                await listPendingNotSolved();
                break;
            case 4:
                await listPendingsSolved();
                break;
            case 5:
                await listIDsAndUserIDs();
                break;
            case 6:
                await listPendingsSolvedWithUser();
                break;
            case 7:
                await listPendingsWithoutResolutionWithUserID();
                break;
            default:
                console.log('Not Valid. Try Again.');
                break;
        }

        await showMenu();
    } else {
        console.log('Quitting the app.');
    }
}

async function optionsUser() {
    return new Promise((resolver) => {
        const readline = require('readline');
        const rl = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        });

        rl.question('Enter the option: ', (answer) => {
            rl.close();
            resolver(parseInt(answer));
        });
    });
}

async function listIDs() {
    const pendings = await obtainPendings();
    const IDs = pendings.map(PENDING => PENDING.id);
    
    console.log('List of all pendings (IDs only: )');
    console.log(IDs);
}

async function listIDsAndTitle() {
    const pendings = await obtainPendings();
    const IDsAndTitle = pendings.map(PENDING => ({ id: PENDING.id, title: PENDING.title }));
    
    console.log('List of all pendings (IDs and Titles):');
    console.log(IDsAndTitle);
}

async function listPendingNotSolved() {
    const pendings = await obtainPendings();
    const notSolved = pendings.filter(PENDING => !PENDING.completed).map(PENDING => ({ id: PENDING.id, title: PENDING.title }));
    
    console.log('List of pendings without resolution (ID and Title):');
    console.log(notSolved);
}

async function listPendingsSolved() {
    const pendings = await obtainPendings();
    const Solved = pendings.filter(PENDING => PENDING.completed).map(PENDING => ({ id: PENDING.id, title: PENDING.title }));
    
    console.log('List of all pendings solved (ID and Titles):');
    console.log(Solved);
}

async function listIDsAndUserIDs() {
    const pendings = await obtainPendings();
    const IDsAndUserIDs = pendings.map(PENDING => ({ id: PENDING.id, userId: PENDING.userId }));
    
    console.log('List of all pendings (IDs and UserID):');
    console.log(IDsAndUserIDs);
}

async function listPendingsSolvedWithUser() {
    const pendings = await obtainPendings();
    const solvedAndUserIDs = pendings.filter(PENDING => PENDING.completed).map(PENDING => ({ id: PENDING.id, userId: PENDING.userId }));
    
    console.log('List of all pendings resolved (ID and UserID):');
    console.log(solvedAndUserIDs);
}

async function listPendingsWithoutResolutionWithUserID() {
    const pendings = await obtainPendings();
    const notSolvedAndUserIDs = pendings.filter(PENDING => !PENDING.completed).map(PENDING => ({ id: PENDING.id, userId: PENDING.userId }));
    
    console.log('List of all pendings without solution (ID and UserID):');
    console.log(notSolvedAndUserIDs);
}

async function main() {
    await showMenu();
}

main();

